import QtQuick 2.0
import QtQuick.Layouts 1.0
import QtQuick.Controls 1.0

Item {
  id: root

  signal initNewGame( size board_size, int mode )


  function getBoardSize() {
    if     (rb_5x5.checked)   return Qt.size(5,5)
    else if(rb_9x9.checked)   return Qt.size(9,9)
    else if(rb_19x19.checked) return Qt.size(19,19)
    else                      return Qt.size(0,0)
  }

  ColumnLayout {
    anchors.fill: parent

    spacing: 20

    Text{
      Layout.alignment: Qt.AlignCenter
      font.family: "Helvetica";
      font.pointSize: 24;
      font.bold: true

      text: "Go Game"
    }

    Text{
      Layout.alignment: Qt.AlignCenter
      font.family: "Helvetica";
      font.pointSize: 20;
      font.bold: false

      text: "Choose the mode to play:"
    }
    Item {
      Layout.alignment: Qt.AlignCenter
      Layout.fillHeight: true
      Layout.fillWidth: true
      ExclusiveGroup { id: boardSizeGroup }
      RowLayout {
          id: rowLayout1
        anchors.fill: parent

        Text {
            font.pointSize: 15;
            text: "Board size: "
        }
        RadioButton{
          smooth: true
          //scale:1.3
          id: rb_5x5
          x: 110
          y: 4
          width: 35
          text: "5x5"
          scale: 1
          antialiasing: true
          transformOrigin: Item.Center
          visible: true
          opacity: 1
          checked: true
          exclusiveGroup: boardSizeGroup
        }
        RadioButton{

          id: rb_9x9
          x: 170
          text: "9x9"
          antialiasing: true
          checked: false
          exclusiveGroup: boardSizeGroup
        }
        RadioButton{

          id: rb_19x19
          x: 230
          text: "19x19"
          z: 0
          antialiasing: true
          checked: false
          exclusiveGroup: boardSizeGroup
        }
      }
    }
    //--

    //--
    Item {

      Layout.alignment: Qt.AlignCenter
      Layout.fillHeight: true
      Layout.fillWidth: true

      RowLayout {
        anchors.fill: parent

        Item {
          Layout.fillHeight: true
          Layout.fillWidth: true

          Button{
            anchors.verticalCenter: parent.verticalCenter
            anchors.horizontalCenter: parent.horizontalCenter

            width: (Math.min( parent.width, parent.height ))/1.5
            height: width/1.5

            text: "vs. Player"
            onClicked: {
              root.initNewGame(
                    getBoardSize(),
                    go.gameModeVsPlayer)
              root.state = "game_board"
            }
          }
        }

        Item {
          Layout.fillHeight: true
          Layout.fillWidth: true

          Button{
            anchors.verticalCenter: parent.verticalCenter
            anchors.horizontalCenter: parent.horizontalCenter

            width: (Math.min( parent.width, parent.height ))/1.5
            height: width/1.5

            text: "vs. AI \n (random - like AI)"
            onClicked: {
              root.initNewGame(
                    getBoardSize(),
                    go.gameModeVsAi)
              root.state = "game_board"
            }
          }
        }

        Item {
          Layout.fillHeight: true
          Layout.fillWidth: true

          Button{
              enabled: false
            anchors.verticalCenter: parent.verticalCenter
            anchors.horizontalCenter: parent.horizontalCenter

            width: (Math.min( parent.width, parent.height ))/1.5
            height: width/1.5
            text: "AI vs. AI"
            onClicked: {
              root.initNewGame(
                    getBoardSize(),
                    go.gameModeAi)
              root.state = "game_board"
            }
          }
        }
      }
    }
  }
}

