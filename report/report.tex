% What kind of text document should we build
\documentclass[a4,10pt]{article}


% Include packages we need for different features (the less, the better)

% Clever cross-referencing
\usepackage{cleveref}

% Math
\usepackage{amsmath}

% Algorithms
\usepackage{algorithm}
\usepackage{algpseudocode}
\usepackage{listings}

% Tikz
\RequirePackage{tikz}
\usetikzlibrary{arrows,shapes,calc,through,intersections,decorations.markings,positioning}

\tikzstyle{every picture}+=[remember picture]

\RequirePackage{pgfplots}

\lstset{language = C++,
		breaklines = true,
		frame = single}

% Set TITLE, AUTHOR and DATE
\title{Report: Implementation of board game Go}
\author{Shvetsov Nikita}
\date{\today}
 
 
\begin{document}

  % Create the main title section
  \maketitle

  \begin{abstract}
    In this report the basic structure of the implemented part of board game Go is presented. The main aim is to
	create fully functional engine for the game, describe the basic structures of the entities, construct methods for
	manipulating of the game data and implement basic rules of Go game. Secondary aims are to create artificial 
	intelligence (AI) algorithms for playing with human player and with another AI type, create multi-threading code 
	for better performance and optimization and also adjust the look of the game.
  \end{abstract}


  %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
  %%  The main content of the report  %%
  %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
 
  \section{Introduction}
	The game of Go is played by two players, Black and White, who consecutively place a stone of their colour
	on an empty intersection of a square grid. Usually the grid contains 19x19 intersections. However the rules
	are flexible enough to accommodate any other board size. Directly neighbouring (connected) stones of the
	same colour form a block. Stones remain fixed throughout the game unless their whole block is captured.
	The directly neighbouring empty intersections of (blocks of) stones are called liberties. A block is captured
	and removed from the board when the opponent places a stone on its last liberty. Moves that do not capture
	an opponent block and leave their own block without a liberty are suicide (if suicide is legal the own block
	is removed). Initially the board is empty, but as the game progresses some stable regions occur which are
	either controlled by Black or by White. A player is allowed to pass. The player who controls most territory
	in the end wins the game~\cite{wref:2003}. Main technologies used to implement the structure of game Go were
	C++ and Qt framework. 
  \section{Technologies}
    C++ is a general-purpose programming language. It has imperative, object-oriented and generic programming 
	features, while also providing facilities for low-level memory manipulation. With the help of C++ object 
	structure of the Go game was implemented. Methods and manipulating data is described in Basic structures 
	implementation section.
	
	Qt is a cross-platform application framework that is widely used for developing application software that 
	can be run on various software and hardware platforms with little or no change in the underlying codebase, 
	while having the power and speed of native applications. Qt is used mainly for developing application software 
	with graphical user interfaces (GUIs). The GUI of Go game was implemented with Qt framework and QML. The
	interface is intuitive, user-friendly and rather simple.
	The basic implementation is shown by image below (see \cref{fig:lena}).
    \begin{figure}[tbp]
      \centering
      \includegraphics[width=0.9\textwidth]{gfx/lena.png}
      \caption{The GUI of Go game (Player vs. Player)}
      \label{fig:lena}
    \end{figure}

  \section{Basic structures and methods}
  During implementation of the game the following classes were constructed: Player, AiPlayer, HumanPlayer, Engine,
  Board (with base class Board\_base), Stone (with base class Stone\_base), Block and Boundary. Board class manipulates
  the data connected with Stone class. Block and Boundary classes stores the information for implementing basic rules
  of the Go game.
  
  During the implementation we use containers from Standard Template Library(STL) of C++ such as \emph{vector}, \emph{set} and \emph{map}. 
  Vector implements an array with fast random access and an ability to automatically resize when appending elements. 
  Sets are containers that store unique elements following a specific order. Maps are associative containers that 
  store elements formed by a combination of a key value and a mapped value, following a specific order~\cite{plauger:2000}.
  Vector containers were used as temporary containers for constructing blocks and boundaries. Sets were used for 
  storing block data and map is used for board data (stones and their location on the board).
  The following source code shows the Block class and its main elements declaration.

  \begin{lstlisting}
class Block
{
public:
    std::set<Point> _block;
    std::shared_ptr<Board> _board;
    std::set<std::shared_ptr<Boundary>> _boundaries;
    StoneColor _color;

    // Constructors
    Block() = default;
    //for 1 stone base block
    explicit Block(Point _block, std::shared_ptr<Board> _board, StoneColor _color);
    //for multiple stones block
    explicit Block(std::set<Point> _block,std::shared_ptr<Board> _board, StoneColor _color,
                   std::set<std::shared_ptr<Boundary>> _boundaries);
    
	std::set<std::shared_ptr<Boundary>> getBoundaries() const;
    std::set<Point> getBlock() const;
    Board getBoard() const;
    StoneColor getColor() const;

    void addBlockPoints(std::set<Point> _points);
};
\end{lstlisting}
  
  From the listing we can see that class Block has 4 data fields, 3 constructors, and 5 methods. These are used
  to implement the main logic structure of the game - the Block.
  
  Although all major rule sets are clear and easily implemented, there are some complicated situations with board
  positions that demand another approach. 
  Since stones can be captured (and removed from the board) it is possible to repeat previous board positions. 
  However, infinite games are not practical, and therefore repetition of positions should be avoided. The basic 
  rule for this situation is \emph{Basic ko}. This only prevents direct repetition in a cycle of two moves. Longer 
  cycles are always allowed~\cite{wref:2003}. The implementation of this situation is to be done.
  
  Suicide situation is also a problem. When placing a stone, due to position of your opponents stones,
  you may encounter situation when your newly placed stone is immediately replaced. This is called suicide move.
  Players should avoid these situations due to it dramatically extends the game time and adds additional free points
  to your opponent. To prevent these situations a solution uses check function \emph{isSuiside()}. Implementation 
  of this function is to be done.
  
  The Go game has non-trivial scoring system. When the game ends positions have to be scored. 
  The two main scoring methods are territory scoring and area scoring. Territory scoring, 
  used by the Japanese rules, counts the surrounded territory plus the number of captured opponent stones. 
  Area scoring, used by the Chinese rules, counts the surrounded territory plus
  the alive stones on the board. The result of the two methods is usually the same up to one point~\cite{wref:2003}.
  Modified Chinese rules were chosen for current project solution.
  Implementation of scoring is to be done. For further work on scoring we can use scoring rule in scientific article
  - ~\cite{wref:2005}.
  
  \section{Results}
	In our solution of the Go game Unit testing was used. Practically for every basic function testing was performed.
	to this moment only basic structure methods were tested such as performing of the engine, placing stones, finding adjacent ones,
	and constructors efficiency. For further plans other tests needed such as rule implementation, block and boundaries constructors
	tests.
	
  \section{Concluding remarks}
  \begin{itemize}
    %\item Reflect over the method and results.
	\item All unit tests of implemented game structure methods were analyzed and passed successfully. This gives us a basis
for further development and testing of the Go game like implementation of the score system, ko-rule and suicide check.	
    \item Future plans include the implementation of advanced GUI, additional go rules, AI heuristics and
	algorithms, based on given difficulty, history functions (like undo move), multi-threading implementation and more.
  \end{itemize}
  
  Overall the base solution is constructed. Main structure functions, methods and GUI are implemented with the use of
  C++11 and Qt with QML. All needed functions were tested with Unit tests successfully. 
  This report was done in cooperation with Umer Jahangir Khan.

  % Include the bibliography
  \bibliographystyle{plain}
  \bibliography{bibliography}

\end{document}
