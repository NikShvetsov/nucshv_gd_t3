
// My Go Engine
#include "../../mylibrary/go.h"

// gtest
#include <gtest/gtest.h>


namespace go = mylib::go;


TEST(GoEngineTest_Board,FromStateConstructor)
{
//Board data visual: Point(x,y) x - col, y - row

//      |x x x x x|
//      |x x x x x|
//      |x x b x x|
//      |x x x x x|
//      |x x x x x|

  go::Board::BoardData board_data;

  // Create a engine from state
  go::Board board {std::move(board_data),go::StoneColor::Black,false};
  board.placeStone(go::Point(3,3));

  // EXPECT a stone at (3,3)
  EXPECT_TRUE(board.hasStone({3,3}));

  // EXPECT a black stone at (3,3)
  EXPECT_TRUE(board.getStone(go::Point{3,3})._color == go::StoneColor::Black);
}

TEST(StoneTest,getNESWofStone) {

    go::Stone _stone = go::Stone{go::Point{2,2}, go::StoneColor::Black, std::shared_ptr<go::Board>(nullptr)};

    //EXPECT North Point to be {2,1}
    EXPECT_TRUE(_stone.getNorth() == (go::Point{2,1}));

    //EXPECT East Point to be {3,2}
    EXPECT_TRUE(_stone.getEast() == (go::Point{3,2}));

    //EXPECT South Point to be {2,3}
    EXPECT_TRUE(_stone.getSouth() == (go::Point{2,3}));

    //EXPECT West Point to be {1,2}
    EXPECT_TRUE(_stone.getWest() == (go::Point{1,2}));
}


TEST(StoneTest,StoneConstructor) {
    //Black stone at {2,2}
    go::Stone _stone = go::Stone{go::Point{2,2}, go::StoneColor::Black, std::shared_ptr<go::Board>(nullptr)};

    //EXPECT Stone Point {2,2}
    EXPECT_TRUE(_stone._point == (go::Point{2,2}));

    //EXPECT StoneColor black
    EXPECT_TRUE(_stone._color == go::StoneColor::Black);
}

TEST(BoardTest,placing_replacingStones) {
    auto engine = std::make_shared<go::Engine>();
    go::Board::BoardData board_data;

    // Create Board data/Position
    go::Board board {std::move(board_data),go::StoneColor::Black,false};

    // Reset data
    board.resetBoard(go::Size{5,5});
    // Placeing stone at {3,3}
    board.placeStone(go::Point{3,3});
    EXPECT_TRUE(board.hasStone(go::Point{3,3}));
    //EXPECT_TRUE(engine->validateStone(go::Point(3,3)));
    // Replacing stone at {3,3}
    board.replaceStone(go::Point{3,3});
    EXPECT_FALSE(board.hasStone(go::Point{3,3}));
}

/*
TEST(GoEngineTest_LegalStonePlacements,SimpleKoRuleTest) {

//      |x x x x x|
//      |x x w x x|
//      |x w b w x|
//      |x b x b x|
//      |x x b x x|

  go::StoneColor B = go::StoneColor::Black;
  go::StoneColor W = go::StoneColor::White;

  go::Board::BoardData b;
                b[{3,2}] = W;
  b[{2,3}] = W; b[{3,3}] = B; b[{4,3}] = W;
  b[{2,4}] = B;               b[{4,4}] = B;
                b[{3,5}] = B;


  // Create a PvP engine
  auto engine = std::make_shared<go::Engine>();
  engine->newGameFromState( std::move(b), go::StoneColor::White, false );

  // Set up the game for the 'ko' rule to trigger:
  // white removes the freedoms of black's Stone on (3,3)
  // and captures it.
  engine->placeStone(go::Point(3,4));

  // Play a illegal move for black triggering the 'ko' rule
  // when black tries to capture whites (3,4).
  EXPECT_FALSE(engine->validateStone(go::Point(3,4)));
}
*/
