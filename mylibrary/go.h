#ifndef GO_H
#define GO_H

//stl
#include <map>
#include <memory>
#include <utility>
#include <chrono>
#include <vector>
#include <set>

namespace mylib {

namespace go {

class Player;
class AiPlayer;
class HumanPlayer;

class Engine;
struct GameState;

class Board;
class Stone;
class Block;
class Boundary;

using time_type = std::chrono::duration<int,std::milli>;

constexpr time_type operator"" _ms (unsigned long long int milli) { return time_type(milli); }
constexpr time_type operator"" _s  (unsigned long long int sec)   { return time_type(sec*1000); }

enum class StoneColor
{
    White       = 0,
    Black       = 1
};

enum class PlayerType
{
    Human       = 0,
    Ai          = 1
};

enum class GameMode
{
    VsPlayer    = 0,
    VsAi        = 1,
    Ai          = 2
};

using Point  = std::pair<int,int>;
using Size   = std::pair<int,int>;

namespace priv
{ //namespace for private classes

class Board_base
{
public:
    using BoardData = std::map<Point,Stone>;
    using BlockData = std::set<std::shared_ptr<Block>>;
    //std::vector<Block> _blocks;
    struct Position
    {
        BoardData     /*std::map<Point,Stone>*/       board;
        StoneColor                                         turn;
        bool                                               was_previous_pass;
        BlockData     /*std::set<std::shared_ptr<Block>>*/ blocks;
        //int freedoms;

        //Constructors
        Position () = default;
        explicit Position ( BoardData&& data, StoneColor turn, bool was_previous_pass);

    }; // END class Position

    // Constructors
    Board_base() = default;
    explicit Board_base( Size size );
    explicit Board_base( BoardData&& data, StoneColor turn, bool was_previous_pass );

    // Board data
    Size        size() const;
    bool        wasPreviousPass() const;
    StoneColor  turn() const;

    // Board
    void        resetBoard( Size size );

    // Board
    Size        _size;
    Position    _current;

}; // END base class Board_base

class Stone_base
{
public:
    StoneColor _color;
    Point _point;
    std::shared_ptr<Board> _board;

    // Constructors
    Stone_base() = default;
    explicit Stone_base(Point _point, StoneColor _color, std::shared_ptr<Board> _board);

    bool hasNorth() const; //indicates if point has stone on NESW
    bool hasEast() const;
    bool hasSouth() const;
    bool hasWest() const;

    Point getNorth() const; //return point of stone
    Point getEast() const;
    Point getSouth() const;
    Point getWest() const;

    StoneColor getColor() const; //get _color, _point, *_board
    Point getPoint() const;
    Board getBoard() const;

    bool isSuiside(); //suicidal rule/check;
};

} // END "private" namespace priv


class Stone: private priv :: Stone_base
{
public:
    using Stone_base::Stone_base;

    using Stone_base::_color;
    using Stone_base::_point;
    using Stone_base::_board;

    using Stone_base::getNorth;
    using Stone_base::getSouth;
    using Stone_base::getEast;
    using Stone_base::getWest;

    using Stone_base::hasNorth;
    using Stone_base::hasSouth;
    using Stone_base::hasEast;
    using Stone_base::hasWest;

    using Stone_base::getColor;
    using Stone_base::getPoint;
    using Stone_base::getBoard;

    using Stone_base::isSuiside;
};

class Block
{
public:
    std::set<Point> _block;
    std::shared_ptr<Board> _board;
    std::set<std::shared_ptr<Boundary>> _boundaries;
    StoneColor _color;

    // Constructors
    Block() = default;
    //for 1 stone base block
    explicit Block(Point _block, std::shared_ptr<Board> _board, StoneColor _color);
    //for multiple stones block
    explicit Block(std::set<Point> _block,std::shared_ptr<Board> _board, StoneColor _color,
                   std::set<std::shared_ptr<Boundary>> _boundaries);
    std::set<std::shared_ptr<Boundary>> getBoundaries() const;

    std::set<Point> getBlock() const;
    Board getBoard() const;
    StoneColor getColor() const;

    void addBlockPoints(std::set<Point> _points);
};

class Boundary {
public:
    std::vector<Point> _points;
    Boundary() = default;
    explicit Boundary(std::vector<Point> _points);

    std::vector<Point> getPoints();
    void replaceAt(Point replacePoint, std::vector<Point> _points);
};

//using BoardData = std::map<Point,Stone_base>;

  // INVARIANTS:
  //   Board fullfills; what does one need to know in order to consider a given position.
  //   * all stones and their position on the board
  //   * who places the next stone
  //   * was last move a pass move
  //   * meta: blocks and freedoms

class Board : private priv::Board_base
{
public:
    // Enable types
    using Board_base::BoardData;
    using Board_base::BlockData;
    using Board_base::Position;

    // Enable constructors
    using Board_base::Board_base;

    // Make visible from Board_base
    using Board_base::resetBoard;
    using Board_base::size;
    using Board_base::wasPreviousPass;
    using Board_base::turn;

    // Validate
    bool           isNextPositionValid( Point intersection ) const;
    bool           isOutOfBounds(Point intersection) const;

    // Actions
    void           placeStone( Point intersection );
    void           passTurn();
    void           replaceStone ( Point intersection);
    void           constructBlocks (Point intersection);

    // Stones and positions
    bool           hasStone( Point intersection ) const;
    Stone     getStone( Point intersection ) const;

    //todo: maintaining blocks and boundries
    std::set<Point> getAdjucentSameColor( Stone intersection ) const;
    void constructBoundaries(Point intersection) const;



}; // END class Board


class Engine : public std::enable_shared_from_this<Engine>
{
public:
    Engine();

    void              newGame( Size size );
    void              newGameVsAi( Size size );
    void              newGameAiVsAi( Size size );
    void              newGameFromState( Board::BoardData&& board, StoneColor turn,
                                                      bool was_previous_pass );

    const Board&      board() const;

    StoneColor        turn() const;

    const GameMode&                 gameMode() const;
    const std::shared_ptr<Player>   currentPlayer() const;

    void              placeStone( Point intersection );
    bool              validateStone( Point intersection ) const;
    void              passTurn();

    void              nextTurn( time_type think_time = 100_ms );
    bool              isGameActive() const;

    void              constructBlocks(Point intersection);

    //todo: maintain blocks and boundries
private:
    Board                     _board;

    GameMode                  _game_mode;
    bool                      _active_game;

    std::shared_ptr<Player>   _white_player;
    std::shared_ptr<Player>   _black_player;

}; // END class Engine


} // END namespace go
} // END namespace mylib

#endif //GO_H
