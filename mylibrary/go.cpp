#include "go.h"
#include "randomai.h"
#include <algorithm>


namespace mylib {
namespace go {

  namespace priv {

//-------------------------------Board_base----------------------------------------

  Board_base::Board_base( Size size )
  {
      resetBoard(size);
  }

  Board_base::Board_base(Board::BoardData&& data, StoneColor turn, bool was_previous_pass)
      : _current{std::forward<Board::BoardData>(data),turn,was_previous_pass}
  {
      // ... init ...
  }

  Board_base::Position::Position(Board::BoardData&& data, StoneColor trn, bool prev_pass)
      : board{data}, turn{trn}, was_previous_pass{prev_pass}, blocks{}
  {
      //...
  }

  void
  Board_base::resetBoard(Size size)
  {
      _current.board.clear();
      _size = size;
      _current.turn = StoneColor::Black;
      _current.blocks.clear();
  }

  Size
  Board_base::size() const
  {
      return _size;
  }

  bool
  Board_base::wasPreviousPass() const
  {
      return _current.was_previous_pass;
  }

  StoneColor
  Board_base::turn() const
  {
      //return _current.turn == StoneColor::Black ? StoneColor::White : StoneColor::Black;
      if (_current.turn == StoneColor::Black)
      {
          return StoneColor::White;
      }
      else
      {
         return StoneColor::Black;
      }
  }

//-------------------------------Stone_base----------------------------------------

  // setting constructor
  Stone_base::Stone_base(Point _point, StoneColor _color, std::shared_ptr<Board> _board)
  {
      this->_point = _point;
      this->_color = _color;
      this->_board = _board;
  }
  //to see hasNorth, hasStone...
  bool
  Stone_base::hasNorth() const
  {
      return ((*(this->_board)).hasStone(getNorth()) && !(*(this->_board)).isOutOfBounds(this->getNorth()));
  }

  bool
  Stone_base::hasEast() const
  {
      return ((*(this->_board)).hasStone(getEast()) && !(*(this->_board)).isOutOfBounds(this->getEast()));
  }

  bool
  Stone_base::hasSouth() const
  {
      return ((*(this->_board)).hasStone(getSouth()) && !(*(this->_board)).isOutOfBounds(this->getSouth()));
  }

  bool
  Stone_base::hasWest() const
  {
      return ((*(this->_board)).hasStone(getWest()) && !(*(this->_board)).isOutOfBounds(this->getWest()));
  }

  //get point of North element in relation to stone
  Point
  Stone_base::getNorth() const
  {
      return Point(_point.first, _point.second-1);
  }

  Point
  Stone_base::getEast() const
  {
      return Point(_point.first+1, _point.second);
  }

  Point
  Stone_base::getSouth() const
  {
      return Point(_point.first, _point.second+1);
  }

  Point
  Stone_base::getWest() const
  {
      return Point(_point.first-1, _point.second);
  }

  bool
  Stone_base::isSuiside()
  {
      //suiside algorithm
  }

  StoneColor
  Stone_base::getColor() const
  {
      return this->_color;
  }

  Point
  Stone_base::getPoint() const
  {
      return this->_point;
  }

  Board
  Stone_base::getBoard() const
  {
      return *(this->_board);
  }

} //End of priv namespace


//-------------------------------Board----------------------------------------

  void
  Board::placeStone(Point intersection)
  {
      if (!this->hasStone(intersection))
     {
          _current.board[intersection] = Stone{intersection, _current.turn, std::shared_ptr<Board>(this)};
          //construct block
          this->constructBlocks(intersection);

          _current.turn = turn(); //swap

          //std::set<Point> newBlockPoints;
          //newBlockPoints.insert(intersection);
          //Block newBlock = Block (newBlockPoints, std::shared_ptr<Board>(this), _current.turn);

          //_current.blocks.insert(std::make_shared<Block>(newBlock));

         //if (!(_current.blocks.count(std::make_shared<Block>(newBlock))==1))
         //{
           //...
         //}


      }

      // construct a block for the newly inserted stone
      // check existing blocks
      // may need to expand or split existing blocks/boundaries
  }

  void Board::constructBlocks(Point intersection)
  {
      std::set<Point> newBlockPoints = {intersection};
      //construct block;

  }

  void Board::replaceStone(Point intersection)
  {
      _current.board.erase(intersection);
  }

  void
  Board::passTurn()
  {
      _current.turn = turn();
  }

  bool
  Board::hasStone(Point intersection) const
  {
      return _current.board.count(intersection);
  }

  //checking of near stones

  Stone
  Board::getStone(Point intersection) const
  {
      if (this->hasStone(intersection))
      {
        return _current.board.at(intersection);
      }
  }

  bool
  Board::isNextPositionValid(Point /*intersection*/) const
  {
      //if (!(this->hasStone(intersection)))
      //{
        return true;
      //}
  }

  bool
  Board::isOutOfBounds(Point intersection) const
  {
      if ( intersection.first > this->size().first || intersection.second > this->size().second )
      {
          return true;
      }
      else
      {
          return false;
      }
  }

  std::set<Point>
  Board::getAdjucentSameColor(Stone intersection) const
  {
      std::set<Point> setOfPoints; //returning set of points

      if (this->isOutOfBounds(intersection.getNorth())
              && this->hasStone(intersection.getNorth())
              && this->getStone(intersection.getNorth()).getColor() == intersection.getColor())
      {
          setOfPoints.insert(intersection.getNorth());
      }

      if (this->isOutOfBounds(intersection.getEast())
              && this->hasStone(intersection.getEast())
              && this->getStone(intersection.getEast()).getColor() == intersection.getColor())
      {
          setOfPoints.insert(intersection.getEast());
      }

      if (this->isOutOfBounds(intersection.getSouth())
              && this->hasStone(intersection.getSouth())
              && this->getStone(intersection.getSouth()).getColor() == intersection.getColor())
      {
          setOfPoints.insert(intersection.getSouth());
      }

      if (this->isOutOfBounds(intersection.getWest())
              && this->hasStone(intersection.getWest())
              && this->getStone(intersection.getWest()).getColor() == intersection.getColor())
      {
          setOfPoints.insert(intersection.getWest());
      }

      return setOfPoints;
  }



//-------------------------------Block----------------------------------------

    // setting constructor for 1 block
    Block::Block(Point _block, std::shared_ptr<Board> _board, StoneColor _color)
    {
        this->_block.insert( _block );
        this->_board = _board;
        this->_color = _color;

        //creating 4th component in time round order
        Boundary newBoundaryPoint = Boundary {std::vector<Point> {
                this->getBoard().getStone(_block).getNorth(),
                this->getBoard().getStone(_block).getEast(),
                this->getBoard().getStone(_block).getSouth(),
                this->getBoard().getStone(_block).getWest()}};
        //adding created vector in Block
        this->_boundaries.insert(std::make_shared<Boundary> (newBoundaryPoint));
    }
    // constructor for set of blocks
    Block::Block(std::set<Point> _block, std::shared_ptr<Board> _board, StoneColor _color, std::set<std::shared_ptr<Boundary> > _boundaries)
    {
        this->_block = _block;
        this->_board = _board;
        this->_color = _color;
        this->_boundaries = _boundaries;
    }

    StoneColor
    Block::getColor() const
    {
        return this->_color;
    }

    Board
    Block::getBoard() const
    {
        return *(this->_board);
    }

    std::set<Point>
    Block::getBlock() const
    {
        return this->_block;
    }

    std::set<std::shared_ptr<Boundary>>
    Block::getBoundaries() const
    {
        return this->_boundaries;
    }

    void
    Block::addBlockPoints(std::set<Point> _points)
    {
        this->_block.insert(_points.begin(), _points.end());
    }



//  Block::Block(std::shared_ptr<Stone> stone_p)
//  {
//      //_stones.push_back(stone_p -> position());
//      //_stones.push_back(stone_p->_point);

//    std::vector<Point> boundry_points;
//    if (!stone_p->hasNorth())
//        boundry_points.push_back(stone_p->North());
//    if (!stone_p->hasEast())
//        boundry_points.push_back(stone_p->East());
//    if (!stone_p->hasSouth())
//        boundry_points.push_back(stone_p->South());
//    if (!stone_p->hasWest())
//        boundry_points.push_back(stone_p->West());

    //n, e, s, w
    //_boundries.push_back(boundry_points);
    //                  Block{}
//  }
//-------------------------------Boundry----------------------------------------
    Boundary::Boundary(std::vector<Point> _points)
    {
        this->_points = _points;
    }

    std::vector<Point>
    Boundary::getPoints()
    {
        return this->_points;
    }

    void
    Boundary::replaceAt(Point replacePoint, std::vector<Point> _points)
    {
        auto findIter = std::find(this->_points.begin(),this->_points.end(), replacePoint);

        //check if findIter found replace point (it returns end, if nothing found)
        if (findIter != this->_points.end())
        {
            this->_points.insert(findIter, _points.begin(), _points.end());
            this->_points.erase(findIter);
        }
    }

//-------------------------------Engine----------------------------------------

  Engine::Engine()
    : _board{}, _game_mode{}, _active_game{false},
      _white_player{nullptr}, _black_player{nullptr}
  {
      //...
  }

  void
  Engine::newGame(Size size)
  {
      _board.resetBoard(size);

      _game_mode = GameMode::VsPlayer;
      _active_game = true;
      _white_player = std::make_shared<HumanPlayer>(shared_from_this(),StoneColor::Black);
      _black_player = std::make_shared<HumanPlayer>(shared_from_this(),StoneColor::White);
  }

  void
  Engine::newGameVsAi(Size size)
  {
      _board.resetBoard(size);

      _game_mode = GameMode::VsAi;
      _active_game = true;

      _white_player = std::make_shared<HumanPlayer>(shared_from_this(),StoneColor::Black);
      _black_player = std::make_shared<RandomAi>(shared_from_this(),   StoneColor::White);
  }

  void
  Engine::newGameAiVsAi(Size size)
  {
      _board.resetBoard(size);

      _game_mode = GameMode::Ai;
      _active_game = true;

      _white_player = std::make_shared<RandomAi>(shared_from_this(),StoneColor::Black);
      _black_player = std::make_shared<RandomAi>(shared_from_this(),StoneColor::White);
  }

  void
  Engine::newGameFromState(Board::BoardData&& board, StoneColor turn, bool was_previous_pass)
  {
      _board = Board {std::forward<Board::BoardData>(board),turn,was_previous_pass};

      _game_mode = GameMode::VsPlayer;
      _active_game = true;
      _white_player = std::make_shared<HumanPlayer>(shared_from_this(),StoneColor::Black);
      _black_player = std::make_shared<HumanPlayer>(shared_from_this(),StoneColor::White);
  }

  const Board&
  Engine::board() const
  {
      return _board;
  }

  const GameMode&
  Engine::gameMode() const
  {
      return _game_mode;
  }

  StoneColor
  Engine::turn() const
  {
      return _board.turn();
  }

  const std::shared_ptr<Player>
  Engine::currentPlayer() const
  {
      if(      turn() == StoneColor::Black ) return _black_player;
      else if( turn() == StoneColor::White ) return _white_player;
      else                                   return nullptr;
  }

  void
  Engine::placeStone(Point intersection)
  {
      _board.placeStone(intersection);
  }

  void
  Engine::passTurn()
  {
      if(board().wasPreviousPass())
      {
          _active_game = false;
          return;
      }
      _board.passTurn();
  }

  void
  Engine::nextTurn(std::chrono::duration<int,std::milli> think_time)
  {
      if( currentPlayer()->type() != PlayerType::Ai) return;

      auto p = std::static_pointer_cast<AiPlayer>(currentPlayer());

      p->think( think_time );
      if( p->nextMove() == AiPlayer::Move::PlaceStone )
          placeStone( p->nextStone() );
      else
          passTurn();
  }

  bool
  Engine::isGameActive() const
  {
      return _active_game;
  }

  bool
  Engine::validateStone(Point pos) const
  {
      return _board.isNextPositionValid(pos);
  }

} // END namespace go
} // END namespace mylib
